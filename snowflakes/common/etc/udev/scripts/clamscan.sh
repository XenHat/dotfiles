#!/usr/bin/env bash
file="/tmp/clamscan.log"
# export DISPLAY=":1"
echo "Starting scan for $1 at $2"

#zenity --info --width=180 --title="Clam Anti Virus" --text="Executing virus scan on flashdrive" &

if [ -f $file ]; then
	rm -f $file
fi

date >$file

# flashdisk_dir=$(udevinfo -q all -p "$(udevinfo -q path -n "${1}")" | grep "ID_FS_LABEL" | awk -F "=" '{print $2}')
flashdisk_dir="$2"

clamscan -r --bell "${flashdisk_dir}"/* | tee $file 2>&1

if [ -f $file ]; then
	file_content=$(
		date
		tail --lines 10 $file
		echo -e "\nSee /tmp/clamscan.log for more information"
	)

	#zenity --text-info --title="Clam Anti Virus" --filename=$file &
	# zenity --info --width=180 --title="Clam Anti Virus" --text="$file_content" &
	echo "$file_content"

fi

exit 0
