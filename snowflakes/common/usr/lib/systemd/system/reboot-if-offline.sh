#!/usr/bin/env bash
check_route() {
	# Make sure your router allows ICMP from LAN or this will fail, bucko!
	if ping -t5 -c 1 _gateway; then
		echo "Gateway replied to our ping"
		return 0 # true
	fi
	echo "No response! Waiting for a bit"
	return 1 # false
}
if ! check_route; then
	sleep 120
	if ! check_route; then
		echo "Still no route! Rebooting!"
		reboot -i
	fi
fi
