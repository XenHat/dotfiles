# prevent breaking distributed compilations
sys-libs/* custom-cflags
sys-devel/* custom-cflags
# mask support for architectures I will never build for
#*/* -llvm_targets_X86 llvm_targets_AVR llvm_targets_Hexagon llvm_targets_Lanai llvm_targets_LoongArch llvm_targets_MSP430 llvm_targets_Mips llvm_targets_NVPTX llvm_targets_PowerPC llvm_targets_Sparc llvm_targets_SystemZ llvm_targets_VE llvm_targets_XCore
# Disable PGO as it breaks distributed compile
*/* pgo
