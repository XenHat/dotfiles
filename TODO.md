# TODO

- Investigate partial hostname match for snowblocks
  - If supported, take advantage of predictable machine hostname convention for config categories
- Investigating moving from zsh to bash, with [Shelldon](https://github.com/rossmacarthur/sheldon) as the plugin manager.
- Investigate using fish if most functionality can be found over there
- use snowblocks/snowblock.json to avoid situations like snowblocks/scripts/scripts for full directory linking
