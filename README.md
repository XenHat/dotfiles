# My dotfiles

## Quick start

Clone the repository and run the `bootstrap` script:

```sh
git clone --depth=1 --recursive https://gitlab.com/XenHat/dotfiles ~/.dotfiles
~/.dotfiles/bootstrap
```

## Goal/Rationale

- Primary focus is a balance of eye-candy, usability and cpu efficiency.
- few to no spinning rust. Untested I/O performance on these devices, although best-effort is attempted every now and then.
- UEFI boot is generally accomplished through my script in scripts/make_unified_image
- Filesystems: Usually F2FS, EXT4, ZFS, or BTRFS

I am currently making use of [Keychain][gentoo_wiki_keychain] for SSH agent management

### General Recommendations

If you use fractional scaling, [avoid GTK apps to prevent performance loss][archwiki_hidpi_fractional_scaling]

- TLP (works better for laptops than `power-profiles-daemon`)
- Run `pwmconfig` on new machines/install for better fan control
- `thermald` can help with heat management
- `s-tui` is a nice monitoring utility and front-end/gui for `stress`
- `btop` is bashtop but in C++

### Some programs I use

- Hardware

  - nvidia, intel and AMD GPUs

- System

  - `clang` compiler. [?][llvm_org_clang]
  - `dbus-broker` (faster dbus implementation)
  - rEFInd as preferred or backup bootloader, systemd-boot otherwise.
  - thermald
  - irqbalance
  - corefreq-cli
  - system76-scheduler instead of gamemode

- CLI

  - `zsh`
  - `tmux`
  - `btop`
  - `git`
    - pre-commit (`pip install pre_commit`) / Husky / lint-staged (currently under review/cleanup)
  - `topgrade` (system update utility)

- Editors

  - `nvim`

- Network

  - iwd (wpa_supplicant replacement)

- GUI/Desktop Environments used

  - KDE + sddm
  - bspwm
    - Polybar
    - Rofi
    - Alacritty
  - Hyprland
    - Waybar
    - Wofi
    - Foot or Konsole

- GUI Programs
  - fsearch as an alternative to baloo and a replacement for Voidtools' Everything search
  - Albert, a MacOS Alfred replacement
  - Firefox
  - Telegram
  - Discord
  - Steam
    - Sunshine + MoonDeckBuddy

A sometimes-updated list of packages I use can be found under [explicit.txt][explicit_txt]

### Additional useful reading

[Use NTP servers from DHCP with systemd-timesyncd][timesyncd_ntp_dhcp]

## TODO

- create menu for installing optional features using [snowsaw][snowsaw_repo] shell commands.
- Global optimization pass
- de-duplication of plugin features
- I/O reduction

[gentoo_wiki_keychain]: https://wiki.gentoo.org/wiki/Keychain#Using_keychain_with_Plasma_5
[timesyncd_ntp_dhcp]: https://wiki.archlinux.org/title/Systemd-timesyncd#Dynamically_set_NTP_servers_received_via_DHCP
[archwiki_hidpi_fractional_scaling]: https://wiki.archlinux.org/title/HiDPI#Fractional_scaling
[llvm_org_clang]: https://clang.llvm.org/
[explicit_txt]: dotfiles/plain/arch/config/xenhat/packages/arch/explicit.txt
[snowsaw_repo]: https://github.com/arcticicestudio/snowsaw
