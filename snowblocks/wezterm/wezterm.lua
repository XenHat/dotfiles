local wezterm = require("wezterm")

-- wezterm.gui is not available to the mux server, so take care to
-- do something reasonable when this config is evaluated by the mux

function get_appearance()
	if wezterm.gui then
		return wezterm.gui.get_appearance()
	else
		return "Dark"
	end
end

function scheme_for_appearance(appearance)
	if appearance:find("Dark") then
		return "3024 Night"
	else
		return "3024 Day"
	end
end

return {
	color_scheme = scheme_for_appearance(get_appearance()),
	tab_bar_at_bottom = false,
	use_fancy_tab_bar = false,
	-- window_decorations = "RESIZE"
	font_size = 10.0,
}
