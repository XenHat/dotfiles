source /usr/share/cachyos-fish-config/cachyos-config.fish
# Ghostty shell integration for Bash. This should be at the top of your bashrc!
if [ -n "$GHOSTTY_RESOURCES_DIR" ]
    builtin source "$GHOSTTY_RESOURCES_DIR"/shell-integration/fish/vendor_conf.d/ghostty-shell-integration.fish
end
# overwrite greeting
# potentially disabling fastfetch
#function fish_greeting
#    # smth smth
#end
