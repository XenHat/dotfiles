#!/usr/bin/env bash
# nvidia-settings
# if [ -f /sys/module/nvidia_drm/version ]; then
# command -v nvidia-settings > /dev/null 2>&1 &&
# nvidia-settings --assign CurrentMetaMode="DP-4: 2560x1440_144 +0+0 {ForceCompositionPipeline=Off, AllowGSYNCCompatible=On}"
# fi
if [[ $(printgpu | wc -l) -gt 1 ]]; then
	active_monitor=$(xrandr -q | tail -n+2 | head -n1 | cut -d ' ' -f 1)
	xrandr --output "$active_monitor" --set "PRIME Synchronization" 1
fi
# xfce4-power-manager &
# picom -b  --focus-exclude "x = 0 && y = 0 && override_redirect = true" &
if command -v 1password; then
	1password --silent &
	disown
fi
lsmod | grep bluetooth && blueman-applet &
#xfce4-power-manager &
killall -s9 -r flameshot
flameshot &
disown
#command -v weechat && choose-terminal -e weechat &
#command -v cadmus && cadmus &
command -v telegram-desktop && killall -s9 -r telegram
telegram-desktop -startintray >/dev/null &
command -v Discord && killall -s9 -r Discord
discord --start-minimized >/dev/null &
# Use 'if cmd; then ..' to check exit code, or 'if [[ $(cmd) == .. ]]' to check output.
# command -v eos-update-notifier && eos-update-notifier &
# command -v eos-arch-news && eos-arch-news &
# ~/.fehbg
# ~/.config/i3/startup.local &
# disown
