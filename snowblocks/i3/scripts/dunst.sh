#!/usr/bin/env bash
command -v dunst || i3-nagbar -t "dunst not found. please install."
envsubst <~/.config/dunst/dunstrc.template >~/.config/dunst/dunstrc
killall -u "${USER}" dunst
dunst &
