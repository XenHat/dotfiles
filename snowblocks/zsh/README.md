# ZSH Dotfiles

ZSH Executes A, then B, then C, etc. The B1, B2, B3 means it executes only the first of those files found.

<!-- trunk-ignore-begin(markdownlint/MD033) -->

|               | Interactive<br> login | Interactive<br> non-login | Script |
| ------------- | --------------------- | ------------------------- | ------ |
| /etc/zshenv   | A                     | A                         | A      |
| ~/.zshenv     | B                     | B                         | B      |
| /etc/zprofile | C                     |                           |        |
| ~/.zprofile   | D                     |                           |        |
| /etc/zshrc    | E                     | C                         |        |
| ~/.zshrc      | F                     | D                         |        |
| /etc/zlogin   | G                     |                           |        |
| ~/.zlogin     | H                     |                           |        |
| ~/.zlogout    | I                     |                           |        |
| /etc/zlogout  | J                     |                           |        |

<!-- trunk-ignore-end(markdownlint/MD033) -->

src: [Startup Files Loading Order][zsh_startup_shreevatsa]
There are five startup files that zsh will read commands from:

```sh
$ZDOTDIR/.zshenv
$ZDOTDIR/.zprofile (if login)
$ZDOTDIR/.zshrc
$ZDOTDIR/.zlogin (if login)
$ZDOTDIR/.zlogout
```

If ZDOTDIR is not set, then the value of `$HOME` is used; this is the usual case.

`.zshenv` is sourced on all invocations of the shell, unless the -f option is set. It should contain commands to set the command search path, plus other important environment variables. `.zshenv` should not contain commands that produce output or assume the shell is attached to a tty.

`.zshrc` is sourced in interactive shells. It should contain commands to set up aliases, functions, options, key bindings, etc.

`.zlogin` is sourced in login shells. It should contain commands that should be executed only in login shells.

`.zlogout` is sourced when login shells exit.

`.zprofile` is similar to `.zlogin`, except that it is sourced before `.zshrc`.

`.zprofile` is meant as an alternative to `.zlogin` for ksh fans; the two are not intended to be used together, although this could certainly be done if desired.

`.zlogin` is not the place for alias definitions, options, environment variable settings, etc.; as a general rule, it should not change the shell environment at all. Rather, it should be used to set the terminal type and run a series of external commands (fortune, msgs, etc).

src: [ZSH Intro, Chapter 3][zsh_intro_3]


### Notes from StackExchange

#### `.zshenv`:

Here a list of what each file should/shouldn't contain, in my opinion:

## .zshenv
_[Read every time]_ 

This file is always sourced, so it should set environment variables which need to be **updated frequently**. _PATH_ (or its associated counterpart _path_) is a good example because you probably don't want to restart your whole session to make it update. By setting it in that file, reopening a terminal emulator will start a new Zsh instance with the _PATH_ value updated.

But be aware that this file is **read even when Zsh is launched to run a single command** (with the _-c_ option), even by another tool like `make`. You should **be very careful to not modify the default behavior of standard commands** because it may break some tools (by setting aliases for example).

## .zprofile
_[Read at login]_

I personally treat that file like `.zshenv` but for commands and variables which should be set once or which **don't need to be updated frequently**:

 - environment variables to configure tools (flags for compilation, data folder location, etc.)
 - configuration which execute commands (like `SCONSFLAGS="--jobs=$(( $(nproc) - 1 ))"`) as it may take some time to execute.

If you modify this file, you can apply the configuration updates by running a login shell:

    exec zsh --login

## .zshrc
_[Read when interactive]_

I put here everything needed only for **interactive usage**:

 - prompt,
 - command completion,
 - command correction,
 - command suggestion,
 - command highlighting,
 - output coloring,
 - aliases,
 - key bindings,
 - commands history management,
 - other miscellaneous interactive tools (auto_cd, manydots-magic)...

## .zlogin

_[Read at login]_

This file is like `.zprofile`, but is read after `.zshrc`. You can consider the shell to be fully set up at .zlogin execution time

So, I use it to launch external commands which do not modify shell behaviors (e.g. a login manager).

## .zlogout

_[Read at logout][Within login shell]_

Here, you can clear your terminal or any other resource which was setup at login.

# How I choose where to put a setting

 - if it is needed by a **command run non-interactively**: `.zshenv`
 - if it should be **updated on each new shell**: `.zshenv`
 - if it runs a command which **may take some time to complete**: `.zprofile`
 - if it is related to **interactive usage**: `.zshrc`
 - if it is a **command to be run when the shell is fully setup**: `.zlogin`
 - if it **releases a resource** acquired at login: `.zlogout`

*src: https://unix.stackexchange.com/a/487889*

[zsh_startup_shreevatsa]: https://shreevatsa.wordpress.com/2008/03/30/zshbash-startup-files-loading-order-bashrc-zshrc-etc/
[zsh_intro_3]: https://zsh.sourceforge.io/Intro/intro_3.html
