return {
	-- you can use the VeryLazy event for things that can
	-- load later and are not important for the initial UI
	-- { "stevearc/dressing.nvim", event = "VeryLazy" },
	-- { "folke/which-key.nvim", event = "VeryLazy", },
	-- { "folke/todo-comments.nvim", event = "VeryLazy"}, 
	-- {
	-- 	"monaqa/dial.nvim",
	-- 	-- lazy-load on keys
	-- 	-- mode is `n` by default. For more advanced options, check the section on key mappings
	-- 	keys = { "<C-a>", { "<C-x>", mode = "n" } },
	-- },
	-- {"nvim-neorg/neorg"},

}
