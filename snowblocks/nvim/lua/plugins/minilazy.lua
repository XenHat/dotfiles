-- Cherry-picked plugins and configs from LazyVim
return {
        {
                "nvim-lualine/lualine.nvim",
                dependencies = { 'nvim-tree/nvim-web-devicons' },
                lazy = false,
                opts = function()
                        -- PERF: we don't need this lualine require madness 🤷
                        local lualine_require = require("lualine_require")
                        lualine_require.require = require

                        vim.o.laststatus = vim.g.lualine_laststatus

                        local opts = {
                                options = {
                                        -- theme = "gruvbox",
                                        globalstatus = vim.o.laststatus == 3,
                                        disabled_filetypes = { statusline = { "dashboard", "alpha", "ministarter", "snacks_dashboard" } },
                                }
                        }
                        return opts
                end,
        },
        -- {
                --         "folke/snacks.nvim",
                --         lazy = false,
                --         -- stylua: ignore
                --         keys = {
                        --                 { "<leader>n", function() Snacks.notifier.show_history() end, desc = "Notification History" },
                        --                 { "<leader>un", function() Snacks.notifier.hide() end, desc = "Dismiss All Notifications" },
                        --         },
                        --         opts = {
                                --                 bigfile = { enabled = true },
                                --                 dashboard = { enabled = true },
                                --                 indent = { enabled = true },
                                --                 input = { enabled = true },
                                --                 notifier = { enabled = true },
                                --                 quickfile = { enabled = true },
                                --                 scroll = { enabled = true },
                                --                 statuscolumn = { enabled = true },
                                --                 words = { enabled = true },
                                --                 dashboard = {
                                        --                         preset = {
                                                --                                 -- header = [[ put your awesome ascii logo here
                                                --                                 -- ]],
                                                --                                 -- stylua: ignore
                                                --                                 ---@type snacks.dashboard.Item[]
                                                --                                 keys = {
                                                        --                                         { icon = " ", key = "f", desc = "Find File", action = ":lua Snacks.dashboard.pick('files')" },
                                                        --                                         { icon = " ", key = "n", desc = "New File", action = ":ene | startinsert" },
                                                        --                                         { icon = " ", key = "g", desc = "Find Text", action = ":lua Snacks.dashboard.pick('live_grep')" },
                                                        --                                         { icon = " ", key = "r", desc = "Recent Files", action = ":lua Snacks.dashboard.pick('oldfiles')" },
                                                        --                                         { icon = " ", key = "c", desc = "Config", action = ":lua Snacks.dashboard.pick('files', {cwd = vim.fn.stdpath('config')})" },
                                                        --                                         { icon = " ", key = "s", desc = "Restore Session", section = "session" },
                                                        --                                         { icon = " ", key = "x", desc = "Lazy Extras", action = ":LazyExtras" },
                                                        --                                         { icon = "󰒲 ", key = "l", desc = "Lazy", action = ":Lazy" },
                                                        --                                         { icon = " ", key = "q", desc = "Quit", action = ":qa" },
                                                        --                                 },
                                                        --                         },
                                                        --                 },
                                                        --         },
                                                        -- }                                                                                                
                                                }
