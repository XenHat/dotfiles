#!/usr/bin/env bash
# version 0.0.3
# THIS IS A WORK IN PROGRESS.
# THIS IS NOT PRODUCTION READY.
# BIG LOUD DISCLAIMER.
# I AM NOT RESPONSIBLE FOR DATA LOSS.
#
# TODO: Add a flag to merge folders considered the same user (i.e. account rename). Perhaps an association, i.e. "folder1:folder2"?
# TODO: handle conversation.log as well
# TODO: merge files with 'Camel Case (group)' format
# TODO: Handle instances of chat logs pasted inside a message...
print_help() {
	echo "Usage: $(basename "$0") [BASE_FOLDER] SOURCE...
	Process files inside SOURCE from BASE_FOLDER and merge them"
	exit 0
}
if [[ -z $1 ]] || [[ $* =~ --help ]]; then
	print_help
fi
# set -x
set -e
set -u
shopt -s extglob globstar nullglob

# How this should work:
# Replace in-message newlines with a control character, to allow sorting
# without breaking multiline messages.
# We then sort them, then undo the substitution.
# This should result in a file that is merged, sorted and with intact multiline
# messages
smart_sort_log_file() {
	source_file=$1
	destination_file=$2
	cat "${source_file}" >>"${destination_file}.step0.buf"
	perl -0777pe 's/\r?\n(?!\[[0-9]{4})//gm' <"${destination_file}.step0.buf" >"${destination_file}.step1.buf"
	sort -u "${destination_file}.step1.buf" --output="${destination_file}.step2.buf"
	sed -E 's//\n/gm' "${destination_file}.step2.buf" >"${destination_file}.txt"
	rm "${destination_file}".*.buf -fv
}

process_parameters() {
	# TODO: re-write this to reduce redundant operations
	oldifs=$IFS
	IFS=":" read -r -a data <<<"${1}"
	IFS=$oldifs
	# Strip trailing slash from first param
	provided_path="${data[0]%%+(/)}"
	full_path_to_first_folder="$(readlink -f "${provided_path}")"
	if [[ ! -d ${full_path_to_first_folder} ]]; then
		exit 1
	fi
	original_source_parent=$(dirname "${full_path_to_first_folder}")
	if [[ ! -d $original_source_parent ]]; then
		echo "parent directory $original_source_parent does not exist!"
		exit 1
	fi
	folder_to_process=$provided_path
	source_folders_list=()
	destination_folder=""
	base_folder_name=$(basename "$folder_to_process")
	echo "=================== $base_folder_name ==================="
	if [[ $folder_to_process =~ : ]]; then
		# TODO: Re-implement different-name merging functionality
		echo "merge mode not implemented yet"
		return
	else
		source_folders_list+=("$folder_to_process")
		destination_folder="$tmpdir/output/${base_folder_name}"
		mkdir -p "${destination_folder}"
	fi
	# TODO: expand instead of looping
	for folder in "${source_folders_list[@]}"; do
		echo "Folder: $folder"
		for file in "${folder}"/*.txt; do
			echo ":: $file"
			if [[ $file =~ ' ' ]]; then
				echo "Filename contains a space, skipped"
				continue
			fi
			if [[ $file =~ synced-ignore ]]; then
				continue
			fi
			base_name=$(basename "$file")
			standard_name="${base_name%%.*}"
			if [[ $file =~ .sync-conflict- ]]; then
				echo "File is a sync conflift and will be merged"
			fi
			smart_sort_log_file "${file}" "$destination_folder/${standard_name}"
		done
	done
}

if [[ -z $1 ]]; then
	echo "You must specify one or more folders to process!"
	exit 2
fi

tmpdir=$(mktemp -d)
mkdir "$tmpdir/source/" -p
mkdir "$tmpdir/output/" -p
process_parameters "$@"
echo "Output: $tmpdir/output"
# echo "$tmpdir/output" | xclip -in
