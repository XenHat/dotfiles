#!/usr/bin/env bash

input_file="$1"
output_suffix="_sliced"
output_directory="${input_file%.*}$output_suffix"
temp_normalized_file="temp_normalized.wav"

# Check if input file is provided
if [ -z "$input_file" ]; then
	echo "Usage: $0 <input_sound_file>"
	exit 1
fi

# Create output directory if it doesn't exist
mkdir -p "$output_directory"

# Check if ffmpeg is installed
if ! command -v ffmpeg &>/dev/null; then
	echo "Error: ffmpeg is not installed. Please install it before running this script."
	exit 1
fi

# Get the duration of the input file in seconds
duration=$(ffmpeg -i "$input_file" 2>&1 | awk '/Duration/ { split($2, a, ":"); print int(a[1]*3600 + a[2]*60 + a[3]) }')

echo "Duration: $duration seconds"

# Apply dynaudnorm to the entire file and save it as a temporary normalized file
ffmpeg -loglevel error -i "$input_file" -af "dynaudnorm" -acodec pcm_s16le -ar 44100 -ac 2 "$temp_normalized_file"

# Calculate the number of 10-second intervals
interval_length=10
num_intervals=$(awk -v dur="$duration" -v int_len="$interval_length" 'BEGIN { print int((dur + int_len - 1) / int_len) }')

echo "Number of intervals: $num_intervals"

# Loop through each interval and extract the segment from the normalized file
for ((i = 1; i <= num_intervals; i++)); do
	start_time=$(((i - 1) * interval_length))
	end_time=$((i * interval_length))
	output_file="$output_directory/segment_$i.wav"

	echo "Processing interval $i: start_time=$start_time, end_time=$end_time, output_file=$output_file"

	ffmpeg -loglevel error -ss "$start_time" -to "$end_time" -i "$temp_normalized_file" -acodec pcm_s16le -ar 44100 -ac 2 "$output_file"
done

# Remove the temporary normalized file
rm -f "$temp_normalized_file"

echo "Slicing and amplification complete. Segments are saved in the '$output_directory' directory."
