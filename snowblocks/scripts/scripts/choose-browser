#!/usr/bin/env bash
# Small utility to pick a web browser based on installed binaries.
# NOTE: This script will break mechanisms such as "Authenticate with Google Login" due to callbacks failing
# inspired by i3-sensible-terminal
# Author: Xenhat Hex <me@xenh.at>
#
# Abort on error to avoid doing very weird or potentially harmful things
set -e
me="$0"
#printf "input parameters: %s" "${*}\n"
params="$*"

_selected_browser=""
# TODO: Use separate list for browser bases to handle parameters more cleanly
# loosely ordered from most respectful to least respectful
# Flatpaks are ordered first because they're easier to remove if unwanted
#shellcheck disable=SC2153
browsers_list=()
# Firefox-based
browsers_list+=(icecat palemoon)
browsers_list+=(net.mullvad.MullvadBrowser mullvad)
browsers_list+=(org.garudalinux.firedragon firedragon)
browsers_list+=(zen-browser)
browsers_list+=(io.gitlab.librewolf-community librewolf)
browsers_list+=(net.waterfox.waterfox waterfox-current waterfox-legacy waterfox)
browsers_list+=(ladybird)
# Mozilla can no longer be trusted
browsers_list+=(org.mozilla.firefox firefox firefox-esr)
# Chromium-based
browsers_list+=(io.github.ungoogled_software.ungoogled_chromium)
browsers_list+=(ungoogled-chromium)
browsers_list+=(brave)
browsers_list+=(chromium)
browsers_list+=(microsoft-edge-dev microsoft-edge-nightly)
browsers_list+=(microsoft-edge-beta microsoft-edge-stable chrome google-chrome)
browsers_list+=(luakit servo)
if [[ -n "$BROWSER" ]]; then
	browsers_list+=("$BROWSER")
fi
for browser in "${browsers_list[@]}"; do
	# Prefer non-bin versions, as it's most likely a user-compiled binary, on purpose.
	[[ $dry_run == true ]] && echo "DEBUG: Testing [$browser]"
	if [[ $browser == "$me" ]] || [[ $browser == "$(basename "$me")" ]]; then
		# echo "DEBUG: skipping self ($me)"
		continue
	elif command -v "${browser}" >/dev/null; then
		_selected_browser="$browser"
		break
	elif command -v "${browser}-bin" >/dev/null; then
		_selected_browser="$browser-bin"
		break
	fi
done

dry_run=false

options=$(getopt -o d,p -l dry-run,private -- "$@")
eval set -- "$options"

while :; do
	case "$1" in
		-d | --dry-run)
			>&2 echo "DEBUG: Dry run"
			echo "$_selected_browser"
			dry_run=true
			# shift
			# OPTION=$1
			;;
		-p | --private)
			>&2 echo "DEBUG: Private"
			;;
		--)
			shift
			break
			;;
	esac

	shift
done

# >&2 echo "got option: $OPTION"
$dry_run && exit 0
if [[ -n "$_selected_browser" ]]; then
	#echo "remaining args are: ${params@Q}"
	# TODO: Add setting to not create new windows on empty url
	# update the shortcut's WM class to hopefully combine tray icons
	# HACK: updating the original file won't work; we need to force updating
	# the plasma cache by creating a copy in-place of the original as it will
	# be detected as a different file and thus work without a relog...
	#_shortcut=$(readlink -f "$HOME/.local/share/applications/choose-browser.desktop")
	_shortcut="$HOME/.local/share/applications/choose-browser.desktop"
	_wm_class=$(which "$_selected_browser")
	# if [[ -f $_shortcut ]]; then
	# 	if ! grep -c "StartupWMClass=$_wm_class" "$_shortcut" >/dev/null; then
	# 		sed -i "s%^StartupWMClass=.*$%StartupWMClass=$_wm_class%" "$_shortcut"
	# 	fi
	# fi
	# if [ "$1" ~= "fox" ] || [ "$1" ~= "fox" ] || [ "$1" ~= "fox" ] || [ "$1" ~= "fox" ] || [ "$1" ~= "fox" ] || [ "$1" ~= "fox" ] || [ "$1" ~= "fox" ] || [ "$1" ~= "fox" ] ||
	# FIXME: re-implement making use of new getopt logic
	# final_args=()
	# for p in $arguments; do
	# 	case $p in
	# 	"--private")
	# 		if [[ $1 =~ "fox" ]] || [[ $1 =~ "firedragon" ]] || [[ $1 =~ "iceweasel" ]] || [[ $1 =~ "icecat" ]] || [[ $1 =~ "palemoon" ]]; then
	# 			final_args+=("--private-window")
	# 		elif [[ $1 =~ "chrom" ]] || [[ $1 =~ "brave" ]] || [[ $1 =~ "edge" ]]; then
	# 			final_args+=("--incognito")
	# 		elif [[ $1 =~ "microsoft-edge" ]]; then
	# 			final_args+=("--inprivate")
	# 		fi
	# 		;;
	# 	*)
	# 		final_args+=("$p")
	# 		;;
	# 	esac
	# done
	# if [ -z "${final_args[*]}" ]; then
	# 	exec "$1" &
	# else
	# 	exec "$1" "${final_args[*]}" &
	# fi
	# disown
	# exit 0
	if [[ -n "$params" ]]; then
		exec "$_selected_browser" "$params"
	else
		exec "$_selected_browser"
	fi
fi
unset browser
unset me
unset launch_browser
exit 1

# TODO: implement --new-window
# FIXME: opens file:// when already running
