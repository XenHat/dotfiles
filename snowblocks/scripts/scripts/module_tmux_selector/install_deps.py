#!/bin/python3
"""install dependencies for tmux_selector"""
import subprocess
import sys

# pip install setuptools if this fails
import pkg_resources
from pkg_resources import DistributionNotFound, VersionConflict


def should_install_requirement(requirement):
    """Check if package needs to be installed"""
    should_install = False
    try:
        pkg_resources.require(requirement)
    except (DistributionNotFound, VersionConflict):
        should_install = True
    return should_install


def install_packages(requirement_list):
    """Install packages from a list"""
    try:
        requirements = [
            requirement
            for requirement in requirement_list
            if should_install_requirement(requirement)
        ]
        if len(requirements) > 0:
            subprocess.check_call(
                [sys.executable, "-m", "pip", "install", *requirements]
            )
        # else:
        # print("Requirements already satisfied.")

    except Exception as e:
        print(e)


def setup():
    """It does the thing. What do you want me to put here?"""
    required_packages = ["simple-term-menu"]
    install_packages(required_packages)
