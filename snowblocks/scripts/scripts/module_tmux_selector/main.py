#!/bin/python3
"""Provides a simple dialog to select TMUX sessions with"""
import os
import subprocess
import sys

sys.path.append(os.path.relpath("~/scripts/util/tmux_selector_setup"))
from tmux_selector_setup import setup

setup()
from simple_term_menu import TerminalMenu


def list_sessions():
    sessions_list = []
    tmux_command_output = ""
    try:
        tmux_command_output = subprocess.check_output(
            ["tmux", "list-sessions", "-F#{session_id}:#{session_name}"],
            universal_newlines=True,
        )
    except subprocess.CalledProcessError:
        pass
    finally:
        for line in tmux_command_output.split("\n"):
            line = line.strip()
            if not line:
                continue
            session_id, session_name = tuple(line.split(":"))
            sessions_list.append((session_name, session_id))
        return sessions_list


def main():
    sessions_list = list_sessions()
    if sessions_list:
        terminal_menu = TerminalMenu(
            ("|".join(session) for session in sessions_list),
            preview_command="tmux capture-pane -e -p -t {}",
            preview_size=0.75,
        )
        menu_entry_index = terminal_menu.show()
        session_name = sessions_list[menu_entry_index][0]
        print(f"Attaching to tmux session '{session_name}'")
        # Create a new randomly-named session sharing the windows but not the
        # active pane, so that each terminal can view a different pane within
        # the same session.
        os.system(
            f"export NEW_SESSION=\"{session_name}-$(uuidgen -t| cut -d '-' -f 1)\" ; tmux new-session -t {session_name} -s $NEW_SESSION ; tmux kill-session -t $NEW_SESSION"
        )


if __name__ == "__main__":
    main()
