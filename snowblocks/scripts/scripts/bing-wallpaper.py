#!/bin/python

# import datetime
import logging
import os
import sys

import dbus
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

images_home = os.path.expanduser("~/Pictures/BingWallpapers")
invalid_chars = [":", "?", "!", "'", " ", "`", "¬", "@", "#", ",", "'", "’"]


def initialise_logger():
    # create logger
    logger = logging.getLogger("logger")
    logger.setLevel(logging.INFO)  # create console handler and set level to debug
    ch = logging.StreamHandler()
    # Set default logging level
    ch.setLevel(logging.INFO)
    formatter = logging.Formatter(
        # "%(asctime)s:%(levelname)s > %(message)s", "%Y-%m-%d %H:%M:%S"
        "%(message)s"
    )
    ch.setFormatter(formatter)
    # add ch to logger
    logger.addHandler(ch)
    return logger


def main():
    global logger
    logger = initialise_logger()

    logger.info("Bing image of the day wallpaper:")
    # logger.debug("Sending request to fetch image.")
    bing_base_url = "https://bing.com"
    bing_url = bing_base_url + "/HPImageArchive.aspx?format=js&idx=0&n=1"
    # logger.info("Bing URL = " + bing_url)

    # make the request for the image from Bing

    response = requests.get(bing_url, headers=None, verify=True, timeout=10)

    if response.status_code == 200:
        # logger.info("Response = 200 (OK)")
        image_path = response.json()["images"][0]["url"]
        image_copyright_desc = response.json()["images"][0]["copyright"]
        image_title = response.json()["images"][0]["title"]

        image_filename = image_title.replace(" ", "_")
        image_filename = image_filename + ".jpg"

        for x in invalid_chars:
            image_filename = image_filename.lower().replace(x, "").strip()

        # logger.info("Image URL = " + bing_base_url + image_path)
        logger.info(image_title + " - " + image_copyright_desc)

        absolute_file_path = images_home + "/" + image_filename

        if not os.path.exists(absolute_file_path):
            if not os.path.exists(images_home):
                os.mkdir(images_home)

            logger.debug("Downloading image.")
            file = open(absolute_file_path, "wb")
            file.write(requests.get(bing_base_url + image_path, timeout=10).content)
            file.close()
            logger.debug("Image saved as = " + absolute_file_path)

        # set wallpaper
        logger.debug("Setting background wallpaper.")

        # connect to dbus and use the plasmashell dbus interface to set wallpaper
        jscript = """
            var allDesktops = desktops();
            print (allDesktops);
            for (i=0;i<allDesktops.length;i++) {
                d = allDesktops[i];
                d.wallpaperPlugin = "%s";
                d.currentConfigGroup = Array("Wallpaper", "%s", "General");
                d.writeConfig("Image", "file://%s")
            }
            """
        bus = dbus.SessionBus()
        plasma = dbus.Interface(
            bus.get_object("org.kde.plasmashell", "/PlasmaShell"),
            dbus_interface="org.kde.PlasmaShell",
        )
        plugin = "org.kde.image"
        plasma.evaluateScript(jscript % (plugin, plugin, absolute_file_path))

        logger.debug("Wallpaper set.")

    else:
        logger.error("Failed to get valid response.")
        logger.error(response.text)
        sys.exit(1)

    sys.exit(0)


if __name__ == "__main__":
    main()
