#!/usr/bin/env bash
# Pick a policy Kit agent and run it
while [[ $found -ne "1" ]]; do
	for agent in /usr/lib64/polkit-kde-authentication-agent-1 /usr/libexec/polkit-gnome-authentication-agent-1 /usr/libexec/polkit-kde-authentication-agent-1 /usr/bin/lxqt-policykit-agent /usr/bin/lxpolkit /usr/lib/mate-polkit/polkit-mate-authentication-agent-1 /usr/bin/polkit-efl-authentication-agent-1 /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 /usr/lib/polkit-kde-authentication-agent-1 /usr/lib/ts-polkitagent /usr/lib/xfce-polkit/xfce-polkit /usr/bin/polkit-dumb-agent; do
		if "$($agent)"; then
			found=1
			break
		fi
	done
	sleep 10
done
