#!/usr/bin/env bash
# Author: Xenhat Hex (me AT xenh.at)
# Dump apple SPI to disk
mfg="apple"
model="macbook"
version="5,2"
# Glob expression to match chips to dump. do not include asterisks, or rework the script.
chip_mfg='Macronix'
chip_model_glob="MX25L"

# =====================================EMBED ===================================
spin() {
	# 			"⣾",
	# "⣽",
	# "⣻",
	# "⢿",
	# "⡿",
	# "⣟",
	# "⣯",
	# "⣷"
	spinner='/|\-/|\-'
	spinner="o 0 8 oo 00 88"
	while :; do
		for i in $(seq 0 7); do
			echo -n "${spinner:i:1}"
			echo -en "\010"
			sleep 0.250
		done
	done
}

# ==============================================================================

function dump_spi() {
	chip_glob="Found ${chip_mfg} flash chip \"${chip_model_glob}"
	dump_folder=$(echo "$HOME/${mfg}_dump/${model}/${version}" | tr ',' '_' | tr '.' '_' | tr ' ' '_')
	for chip in $(sudo flashrom -p internal 2>&1 | grep "${chip_glob}" | cut -d ' ' -f 5 | tr -d '"'); do
		echo "Dumping ${chip}..."
		spin &
		SPIN_PID=$!
		trap 'kill -9 $SPIN_PID' $(seq 0 15)
		dst_file=${dump_folder}/"$(echo "${chip}" | tr '/' '.').bin"
		sudo flashrom -p internal -c "${chip}" -r "${dst_file}" >/dev/null 2>&1
		# Kill leftovers. Savage but effective.
		kill -9 $SPIN_PID 2>/dev/null
		fddd="${dump_folder}/fd_dump/$(basename "${dst_file}")"
		mkdir -p "${fddd}" && pushd "${fddd}" >/dev/null && sudo ifdtool -x "${dst_file}" >/dev/null
		popd >/dev/null || exit
	done
}
dump_spi
