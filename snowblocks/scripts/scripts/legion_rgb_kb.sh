#!/usr/bin/env sh
echo '{"name":"Dim white","rgb_zones":[{"rgb":[22,22,22],"enabled":true},{"rgb":[22,22,22],"enabled":true},{"rgb":[22,22,22],"enabled":true},{"rgb":[22,22,22],"enabled":true}],"effect":"Static","direction":"Left","speed":1,"brightness":"Low"}' >"$XDG_RUNTIME_DIR/legion-kb-profile"
legion-kb-rgb load-profile --path "$XDG_RUNTIME_DIR/legion-kb-profile"
