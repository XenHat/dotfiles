#!/usr/bin/env bash

if [[ -r ~/code/xenhat/yaowg/yaowg.bash ]]; then
	~/code/xenhat/yaowg/yaowg.bash
fi

# FIXME: This script keeps getting called via app-polkit-gnome-authentication-agent-1@autostart.service when restarting sunshine???
# start_nso_rpc() {
# 	_nso_location="$HOME/src/NSO-RPC/client"
# 	if [ -f "${_nso_location}/app.py" ]; then
# 		for wait_time in 1 .. 5; do
# 			pgrep -u "$USER" -a Discord && sleep 2
# 			# Fix icon until the path fix is merged
# 			#python3 "${_nso_location}/app.py"
# 			if pushd "$_nso_location"; then
# 				pgrep -f 'app.py' || python3 app.py &
# 			else
# 				return
# 			fi
# 			break
# 			sleep 1
# 			((wait_time + 1))
# 		done
# 	fi
# }
#
start_openrgb() {
	if command -v openrgb; then
		openrgb --autostart-disable --profile default &
	fi
}
# plex_rich_presence() {
# 	prp_path="$HOME/Downloads/PlexRichPresence/usr/share/PlexRichPresence.UI.Avalonia/PlexRichPresence.UI.Avalonia"
# 	[[ -f $prp_path ]] && "$prp_path" --minimized &
# }

# TODO: make this variable maybe?
# gsettings set org.gnome.desktop.interface color-scheme prefer-dark
# command -v 1password && 1password --silent &
# disown
# command -v albert >/dev/null 2>&1 && albert &
# disown
start_openrgb &
disown
# if [[ "$(hostnamectl hostname)" == "gig19ws001" ]]; then
# 	if ! pgrep sunshine; then
# 		systemctl --user restart sunshine
# 	fi
# 	if ! pgrep -f moondeckbuddyservice; then
# 		systemctl --user restart moondeckbuddy
# 	fi
# fi
#
