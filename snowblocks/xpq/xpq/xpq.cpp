#include <exception>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>

// TODO: use libpci to list/count gpus instead of hard-coding the number

std::string readquick(std::string filename) {
  std::ifstream is(filename);
  std::stringstream buffer;
  buffer << is.rdbuf();
  std::string out = buffer.str();
  return out.erase(out.find_last_not_of("\r"));
}

std::string translateVendorID(std::string id) {
  if (id == "0x10de") {
    return "NVIDIA";
  }
  if (id == "0x1002") {
    return "AMD";
  }
  if (id == "0x8086") {
    return "INTEL";
  }
  return "Unknown";
}

typedef std::pair<std::string, std::string> prop_pair;

bool getGPUProps(int index, prop_pair &out) {
  // std::vector<std::string> indicator_symbol = {'+', '-'};
  std::string path = "/sys/class/drm/card" + std::to_string(index);
  if (!std::filesystem::exists(path)) {
    return false;
  }
  std::string vendor_id_stub = "/device/vendor";
  std::string vendor_full_path = path + vendor_id_stub;
  auto first_line_vendor = readquick(vendor_full_path);
  auto vendor_id = translateVendorID(first_line_vendor);

  std::string power_state_stub = "/device/power_state";
  std::string power_full_path = path + power_state_stub;
  std::string power_state = readquick(power_full_path);

  out.first = vendor_id;
  out.second = power_state;
  return true;
}
int main(int argc, char **argv) {
  // Read power usage from the battery subsystem
  auto wat = readquick("/sys/class/power_supply/BAT0/power_now");
  float power_now_raw = std::strtol(wat.c_str(), NULL, 10);
  float power_now_watts = (power_now_raw * 0.000001);
  std::cout << printf("%.1f", power_now_watts) << " W/h";
  std::cout << " ";

  for (int i = 0; i < 2; i++) {
    try {
      prop_pair props;
      if (getGPUProps(i, props)) {
        std::cout << props.first << ": " << props.second << " ";
      }
    } catch (std::exception) {
      // do nothing
    }
  }
  std::cout << std::endl;
  return 0;
}
