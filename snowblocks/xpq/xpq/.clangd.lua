require("lspconfig").clangd.setup({
    cmd = {
        "clangd",
        "--pretty",
        "--header-insertion=iwyu",
        "--background-index",
        "--suggest-missing-includes",
		"--query-driver=/usr/**/bin/clang-*",
        "-j=8",
        "--pch-storage=memory",
        "--clang-tidy",
        "--compile-commands-dir=.",
    },
    filetypes = { "c", "cpp", "objc", "objcpp", "cuda", "proto" },
})
