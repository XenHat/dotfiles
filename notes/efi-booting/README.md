# Notes about EFI booting

## Preface

The [Arch Linux Wiki][archwikimainpage] is a great resource, but sometimes the information required to perform a task from the ground-up is scattered across several pages and subsections.

Here is an attempt to consolidate the information I needed to configure my computer's boot process the way I desired.

## ESP partition (a.k.a. /boot/efi or /efi)

The new recommended [ESP][wiki_esp] mount path is `/boot`:

> pacman will directly update the kernel that the EFI firmware will read if you mount the ESP to /boot.[¹][1]
>
> The bootloader must be able to read _/boot_ by itself without relying on kernel modules.[²][2]

- The easy answer for `systemd-boot` is to install [efifs][efifs] and then copying the required filesystem drivers to `<esp>/EFI/systemd/drivers`:

  ```bash
  sudo mkdir /boot/EFI/systemd/drivers -p && \
  sudo cp /usr/lib/efifs-x64/{btrfs,exfat,f2fs,ntfs,zfs}_x64.efi \
  /boot/EFI/systemd/drivers/
  ```

  Using the method above, boot time can be reduced significantly by using **btrfs** as _it does not require a mandatory fsck at boot_.

  It is generally recommended to use `rootflags=subvol=/path/to/subvolume` in
  the bootloader (or kernel) configuration to avoid issues with the `objectid`
  changing after a btrfs snapshot restoration.

  - `/ect/fstab` format is the same, simply omit `rootflags=`; i.e.

    ```fstab
    /dev/disk/by/partlabel/linux_btrfs /mnt/newroot btrfs defaults,subvol=/sysroot 0 0
    /dev/disk/by/partlabel/linux_btrfs /mnt/newroot/etc btrfs defaults,subvol=/sysroot/etc 0 0
    /dev/disk/by/partlabel/linux_btrfs /mnt/newroot/home btrfs defaults,subvol=/home 0 0
    ```

  - the mount commands for the above would then be:

    ```bash
    sudo mount /dev/disk/by-partlabel/linux_main_btrfs -o defaults,subvol=/sysroot /mnt/newroot
    # This should get mounted implicitly, but we can mount it explicitly
    sudo mount /dev/disk/by-partlabel/linux_main_btrfs -o defaults,subvol=/sysroot/etc /mnt/newroot/etc
    # mixed-hierarchy example, because I like to keep /home separate in the event of a recursive mishap.
    sudo mount /dev/disk/by-partlabel/linux_main_btrfs -o defaults,subvol=/home /mnt/newroot/home
    ```

  - on SSD, you can reduce commit latency ("write lag") with the `discard=async` mount option starting with **Linux 5.6**.[4][ssdasync]

  - it is possible to remove the need to specify `subvol=` for `/` by running `btrfs subvolume set-default subvolume-id /` **inside the chroot or booted system**.[³][3]

[archwikimainpage]: https://wiki.archlinux.org/
[wiki_esp]: https://wiki.archlinux.org/title/EFISTUB#Preparing_for_EFISTUBnded
[1]: https://wiki.archlinux.org/title/EFISTUB#Preparing_for_EFISTUB
[2]: https://wiki.archlinux.org/title/Arch_boot_process#Feature_comparison
[efifs]: https://archlinux.org/packages/?name=efifs
[3]: https://wiki.archlinux.org/title/btrfs#Changing_the_default_sub-volume

<!-- [volidfragile]: https://wiki.archlinux.org/title/btrfs#Mounting_subvolume_as_root -->

[ssdasync]: https://wiki.archlinux.org/title/btrfs#SSD_TRIM
